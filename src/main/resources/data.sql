-- Companies
insert into company(company_id, created_at, updated_at, doing_business_as, owner_in__contact_id) 
	values (1, now(), now(), 'RCS Inc.', null);

-- Contacts
insert into contact(contact_id, created_at, updated_at, name, email, company_id) 
	values (1, now(), now(), 'Rafael Cechinel Silvestri', 'rafaelcechinel@gmail.com', 1);
insert into contact(contact_id, created_at, updated_at, name, email, company_id) 
	values (2, now(), now(), 'Vanessa Cust�dia Martins', 'vanessacustoriamartins@gmail.com', 1);
insert into contact(contact_id, created_at, updated_at, name, email, company_id) 
	values (3, now(), now(), 'Lara Martins Silvestri', null, 1);
	
update company set owner_in__contact_id=1 where company_id=1;
	
-- Vendors
insert into vendor(vendor_id, created_at, updated_at, company_id, contact_in__contact_id) 
	values (1, now(), now(), 1, 2);
	
-- Categories
insert into category(category_id, created_at, updated_at, company_id, name, is_active) 
	values (1, now(), now(), 1, 'Food', true);
insert into category(category_id, created_at, updated_at, company_id, name, is_active) 
	values (2, now(), now(), 1, 'Car', true);
insert into category(category_id, created_at, updated_at, company_id, name, is_active) 
	values (3, now(), now(), 1, 'Restaurant', true);
