package com.raccoon.expenses.health.controller;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Rafael Silvestri
 *
 */

@RestController
@RequestMapping("/v1/health")
public class HealthV1Controller {

	@GetMapping
	@Produces(MediaType.APPLICATION_JSON)
	public Response resourceCheck() {
		return Response.ok("OK").build();
	}
}
