package com.raccoon.expenses.service;

/**
 * 
 * @author Rafael Silvestri
 *
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.raccoon.expenses.model.Category;
import com.raccoon.expenses.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	/*
	 * 
	 */
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Category getById(Long id) {
		return categoryRepository.findOne(id);
	}

	/**
	 * 
	 * @param category
	 * @return
	 */
	public Category create(Category category) {
		return categoryRepository.save(category);
	}

	/**
	 * 
	 * @param category
	 * @return
	 */
	public Category update(Category category) {
		return categoryRepository.save(category);
	}

	/**
	 * 
	 * @param id
	 */
	public void delete(Long id) {
		categoryRepository.delete(id);
	}

}
