package com.raccoon.expenses.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.raccoon.core.model.BaseEntity;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@Entity
@Table(name = "company")
public class Company extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "company_id")
	private Long id;

	@NotNull
	@Column(name = "doing_business_as")
	private String doingBusinessAs;

	@OneToOne
	@JoinColumn(name = "owner_in__contact_id")
	private Contact owner;

	public Company() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDoingBusinessAs() {
		return doingBusinessAs;
	}

	public void setDoingBusinessAs(String doingBusinessAs) {
		this.doingBusinessAs = doingBusinessAs;
	}

	public Contact getOwner() {
		return owner;
	}

	public void setOwner(Contact owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", doingBusinessAs=" + doingBusinessAs + ", owner=" + owner + "]";
	}

}
