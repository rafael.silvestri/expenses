package com.raccoon.expenses.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.raccoon.core.model.BaseEntity;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@Entity
@Table(name = "contact")
public class Contact extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "contact_id")
	private Long id;

	@Column(name = "company_id")
	private Long companyId;

	@NotNull
	@Column(name = "name")
	private String name;

	@Column(name = "email")
	private String email;

	public Contact() {
	}

	public Long getId() {
		return id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", name=" + name + ", email=" + email + "]";
	}

}
