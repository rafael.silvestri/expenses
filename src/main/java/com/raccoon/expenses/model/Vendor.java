package com.raccoon.expenses.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.raccoon.core.model.BaseEntity;

/**
 * 
 * @author Rafael Silvestri
 *
 */

@Entity
@Table(name = "vendor")
public class Vendor extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "vendor_id")
	private Long id;

	@Column(name = "company_id")
	private Long companyId;

	@OneToOne
	@JoinColumn(name = "contact_in__contact_id")
	private Contact contact;

	public Vendor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", contact=" + contact + "]";
	}

}
