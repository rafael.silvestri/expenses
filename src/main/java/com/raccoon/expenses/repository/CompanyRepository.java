package com.raccoon.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raccoon.expenses.model.Company;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

}
