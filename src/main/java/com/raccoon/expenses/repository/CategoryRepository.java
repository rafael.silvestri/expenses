package com.raccoon.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raccoon.expenses.model.Category;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
