package com.raccoon.expenses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raccoon.expenses.model.Contact;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>{

	public Contact findContactById(Long id);
	
}
