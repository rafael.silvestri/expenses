package com.raccoon.expenses.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.raccoon.expenses.model.Contact;
import com.raccoon.expenses.repository.ContactRepository;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@RestController
@RequestMapping("/v1/contacts")
public class ContactV1Controller {	

	@Autowired
	ContactRepository contactRepository;

	@GetMapping
	public Response getAll() {
		List<Contact> contacts = contactRepository.findAll();
		return Response.ok(contacts).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Contact> getById(@PathVariable(value = "id") Long id) {
		Contact contact = contactRepository.findContactById(id);
		return ResponseEntity.ok().body(contact);
	}
	
	@PostMapping
	public Contact create(@Valid @RequestBody Contact contact) {
		return contactRepository.save(contact);
	}

}
