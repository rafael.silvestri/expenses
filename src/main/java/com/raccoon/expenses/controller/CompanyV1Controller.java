package com.raccoon.expenses.controller;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.raccoon.expenses.model.Company;
import com.raccoon.expenses.repository.CompanyRepository;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@RestController
@RequestMapping("/v1/companies")
public class CompanyV1Controller {

	@Autowired
	CompanyRepository companyRepository;

	@GetMapping
	public Response getAll() {
		List<Company> companies = companyRepository.findAll();
		return Response.ok(companies).build();
	}

}
