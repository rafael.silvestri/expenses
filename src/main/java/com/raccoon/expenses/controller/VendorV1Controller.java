package com.raccoon.expenses.controller;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.raccoon.expenses.model.Vendor;
import com.raccoon.expenses.repository.VendorRepository;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@RestController
@RequestMapping("/v1/vendors")
public class VendorV1Controller {

	@Autowired
	VendorRepository vendorRepository;

	@GetMapping
	public Response getAll() {
		List<Vendor> vendors = vendorRepository.findAll();
		return Response.ok(vendors).build();
	}

}
