package com.raccoon.expenses.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.raccoon.expenses.dto.CategoryV1Dto;
import com.raccoon.expenses.model.Category;
import com.raccoon.expenses.service.CategoryService;

/**
 * 
 * @author Rafael Silvestri
 *
 */
@RestController
@RequestMapping("/v1/categories")
public class CategoryV1Controller {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public List<CategoryV1Dto> getCategories() {
		List<Category> categories = categoryService.findAll();
		return categories.stream().map(this::convertToDto).collect(Collectors.toList());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public CategoryV1Dto create(@RequestBody CategoryV1Dto categoryDto) {
		Category category = convertToEntity(categoryDto);
		Category categoryCreated = categoryService.create(category);
		return convertToDto(categoryCreated);
	}

	@GetMapping(value = "/{id}")
	@ResponseBody
	public CategoryV1Dto getCategory(@PathVariable("id") Long id) {
		return convertToDto(categoryService.getById(id));
	}

	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public CategoryV1Dto update(@RequestBody CategoryV1Dto categoryDto) {
		Category category = convertToEntity(categoryDto);
		Category categoryUpdated = categoryService.update(category);
		return convertToDto(categoryUpdated);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {
		categoryService.delete(id);
	}

	// --------------------------------------------------------------------------
	private CategoryV1Dto convertToDto(Category category) {
		return modelMapper.map(category, CategoryV1Dto.class);
	}

	private Category convertToEntity(CategoryV1Dto categoryDto) {
		// TODO: entender isso melhor?
		Category category = modelMapper.map(categoryDto, Category.class);
		if (categoryDto.getId() != null) {
			Category oldCategory = categoryService.getById(categoryDto.getId());
			// merge with new dto
		}
		return category;
	}

}
