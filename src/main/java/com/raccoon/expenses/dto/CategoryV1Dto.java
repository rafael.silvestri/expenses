package com.raccoon.expenses.dto;

/**
 * 
 * @author Rafael Silvestri
 *
 */
public class CategoryV1Dto {
	private Long id;
	private String name;
	private boolean active;

	public CategoryV1Dto() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
